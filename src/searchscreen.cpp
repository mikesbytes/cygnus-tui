//
// Created by user on 1/1/20.
//

#include <cpptui/cpptui.hpp>
#include "searchscreen.hpp"
#include "interface.hpp"

SearchScreen::SearchScreen(ClientCtxPtr ctx) :
    model_(ctx),
    list_(model_)
{
    reflow();
}

void SearchScreen::draw() {
    if (!isActive()) return;

    tui::clear();

    list_.draw();

    search_.setText(search_query_);
    search_.draw();

    tui::present();
}

void SearchScreen::processEvent(const std::pair<ClientCtxPtr, nlohmann::json> &ev) {

}

void SearchScreen::processEvent(const tui::Event &ev) {
    bool do_draw = false;
    if (ev.type == tui::EventType::RESIZE) {
        reflow();
        do_draw = true;
    }

    if (!isActive()) return;

    if (getInterface()->getFocus()) {
        if (ev.key == tui::KeyCode::ESC) {
            getInterface()->setFocus(false);
        } else if (ev.str() != "") {
            search_query_ += ev.str();
            model_.doSearch(search_query_);
            do_draw = true;
        } else if (ev.key == tui::KeyCode::BACKSPACE) {
            search_query_ = search_query_.substr(0, search_query_.size() - 1);
            model_.doSearch(search_query_);
            do_draw = true;
        }
    } else {
        if (ev.str() == "s") {
            getInterface()->setFocus(true);
        } else {
            do_draw = list_.processEvent(ev) || do_draw;
        }
    }

    if (do_draw) {
        draw();
    }
}

void SearchScreen::reflow() {
    auto sz = tui::window_size();
    list_.setPos(tui::Pos(0,0));
    list_.setSize(tui::Size(sz.x, sz.y - 1));

    search_.setPos(tui::Pos(0, sz.y - 1));
    search_.setSize(tui::Size(sz.x, 1));
}

