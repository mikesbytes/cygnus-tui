//
// Created by user on 12/29/19.
//

#ifndef CYGNUS_TUI_WIDGET_HPP
#define CYGNUS_TUI_WIDGET_HPP

#include "cpptui/ivec2.hpp"

class Widget {
public:
    virtual void draw() = 0;

    const tui::Pos &getPos() const;
    void setPos(const tui::Pos &pos);

    const tui::Size &getSize() const;
    void setSize(const tui::Size &size);

private:
    tui::Pos pos_;
    tui::Size size_;
};


#endif //CYGNUS_TUI_WIDGET_HPP
