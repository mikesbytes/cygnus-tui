//
// Created by user on 12/31/19.
//

#ifndef CYGNUS_TUI_BROWSESCREEN_HPP
#define CYGNUS_TUI_BROWSESCREEN_HPP

#include "screen.hpp"
#include "widgets/itemviewwidget.hpp"
#include "browsemodel.hpp"

class BrowseScreen : public Screen {
public:
    BrowseScreen(ClientCtxPtr ctx);

    void draw() override;

    void processEvent(const std::pair<ClientCtxPtr, nlohmann::json> &ev) override;
    void processEvent(const tui::Event &ev) override;

private:
    void reflow();

    ClientCtxPtr ctx_;
    BrowseModel model_;
    ItemViewWidget list_;
};


#endif //CYGNUS_TUI_BROWSESCREEN_HPP
