//
// Created by user on 1/1/20.
//

#include <fmt/format.h>
#include "searchmodel.hpp"

SearchModel::SearchModel(ClientCtxPtr ctx) :
    ctx_(ctx)
{

}

std::optional<Item> SearchModel::getItem(const ItemIndex &idx) {
    try {
        Item ret;
        // enqueue results
        if (idx.x == 0) {
            // artists column
            ret.text = data_.at("artists").at(idx.y);
            return ret;
        } else if (idx.x == 1) {
            ret.text = data_.at("albums").at(idx.y).at("title");
            return ret;
        } else if (idx.x == 2) {
            auto j = data_.at("tracks").at(idx.y);
            ret.text = j.at("title");
            return ret;
        }
    } catch (std::exception &e) {

    }
    return std::nullopt;
}

int SearchModel::columnCount() {
    return 3;
}

int SearchModel::rowCount() {
    try {
        return std::max(data_.at("artists").size(), std::max(data_.at("albums").size(), data_.at("tracks").size()));
    } catch (std::exception &e) {

    }
    return 0;
}

CursorMode SearchModel::cursorMode() {
    return CursorMode::ITEM;
}

int SearchModel::columnWidth(int idx, int total_width) {
    return total_width / 3;
}

bool SearchModel::processEvent(const tui::Event &ev, const tui::IVec2 &cursor) {
    if (ev.str() == " ") {
        // enqueue
        try {
            json j = {{"message-type", "action"},
                      {"action",       "enqueue"},
                      {"data",         {}}};

            // add artist
            if (cursor.x == 0) {
                j["data"]["artist"] = data_.at("artists").at(cursor.y);
            } else if (cursor.x == 1) {
                // add album
                j["data"]["album-id"] = data_.at("albums").at(cursor.y).at("id");
            } else if (cursor.x == 2) {
                // add track
                j["data"]["uris"] = {};
                j["data"]["uris"].push_back(data_.at("tracks").at(cursor.y).at("uri"));
            }

            ctx_->send(j);
        }
        catch (const std::exception &e) {
            // TODO: log this
        }
    }
    return false;
}

bool SearchModel::doSearch(const std::string &query) {
    try {
        Request r(json{{"message-type","action"},
                       {"action","library-search"},
                       {"data",{
                                               {"mode","all"},
                                               {"query",fmt::format("%{}%", query)}
                                       }}});
        ctx_->processRequest(r);
        auto response = r.getResponse(std::chrono::milliseconds(200));
        if (response != std::nullopt) {
            auto j = response->getJSON();
            if (j.at("status") == "success") {
                data_ = response->getJSON().at("data");
                return true;
            }
        }
    } catch (std::exception &e) {
        // TODO: log exception here
    }
    return false;
}
