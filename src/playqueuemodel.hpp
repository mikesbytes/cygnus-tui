//
// Created by user on 12/29/19.
//

#ifndef CYGNUS_TUI_PLAYQUEUEMODEL_HPP
#define CYGNUS_TUI_PLAYQUEUEMODEL_HPP


#include <nlohmann/json.hpp>
#include <trackmetadata.hpp>
#include <clientcontext.hpp>
#include "itemviewmodel.hpp"

class PlayQueueModel : public ItemViewModel{
public:
    PlayQueueModel(ClientCtxPtr ctx);

    std::optional<Item> getItem(const ItemIndex &idx) override;

    int columnCount() override;

    int rowCount() override;

    CursorMode cursorMode() override;

    int columnWidth(int idx, int total_width) override;

    bool processEvent(const tui::Event &ev, const tui::IVec2 &cursor) override;

    void fullUpdate(const nlohmann::json &data);
    void partialUpdate(const nlohmann::json &data);
    void setIndex(int idx);

private:
    std::vector<TrackMetadata> tracks_;
    int index_;
    ClientCtxPtr ctx_;
};


#endif //CYGNUS_TUI_PLAYQUEUEMODEL_HPP
