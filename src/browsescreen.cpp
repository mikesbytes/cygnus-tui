//
// Created by user on 12/31/19.
//

#include <cpptui/cpptui.hpp>
#include "browsescreen.hpp"

BrowseScreen::BrowseScreen(ClientCtxPtr ctx) :
    ctx_(ctx),
    model_(ctx),
    list_(model_)
{
    reflow();
}

void BrowseScreen::draw() {
    if (!isActive()) return;
    tui::clear();

    list_.draw();

    tui::present();
}

void BrowseScreen::processEvent(const std::pair<ClientCtxPtr, nlohmann::json> &ev) {
}

void BrowseScreen::processEvent(const tui::Event &ev) {
    bool do_draw = false;
    if (ev.type == tui::EventType::RESIZE) {
        reflow();
        do_draw = true;
    }

    if (!isActive()) return;

    do_draw = list_.processEvent(ev) || do_draw;

    if (do_draw) {
        draw();
    }
}

void BrowseScreen::reflow() {
    auto sz = tui::window_size();

    list_.setPos(tui::Pos(0,0));
    list_.setSize(sz);
}
