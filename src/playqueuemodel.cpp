//
// Created by user on 12/29/19.
//

#include <cpptui/cpptui.hpp>
#include "playqueuemodel.hpp"
#include "metadatatools.hpp"

PlayQueueModel::PlayQueueModel(ClientCtxPtr ctx) :
    ctx_(ctx)
{

}

std::optional<Item> PlayQueueModel::getItem(const ItemIndex &idx) {
    if (idx.x < 0 || idx.x >= 4) { return std::nullopt; }
    if (idx.y < 0 || idx.y >= tracks_.size()) { return std::nullopt; }

    Item ret;
    ret.format = "";
    if (idx.x == 0) {
        ret.format = tui::format_string(tui::FormatCode::YELLOW);
        ret.text = tracks_[idx.y].artist;
    } else if (idx.x == 1) {
        ret.text = tracks_[idx.y].title;
    } else if (idx.x == 2) {
        ret.text = std::to_string(tracks_[idx.y].track);
        ret.format = tui::format_string(tui::FormatCode::GREEN);
    } else if (idx.x == 3) {
        ret.text = tracks_[idx.y].album;
        ret.format = tui::format_string(tui::FormatCode::CYAN);
    }

    if (idx.y == index_) {
        ret.format = tui::format_string(tui::FormatCode::BOLD) + tui::format_string(tui::FormatCode::RED);
    }

    return ret;
}

int PlayQueueModel::columnCount() {
    return 4;
}

int PlayQueueModel::rowCount() {
    return tracks_.size();
}

CursorMode PlayQueueModel::cursorMode() {
    return CursorMode::ROW;
}

int PlayQueueModel::columnWidth(int idx, int total_width) {
    if (idx == 0) return 25;
    if (idx == 1) return total_width - 25 - 35 - 4;
    if (idx == 2) return 4;
    if (idx == 3) return 35;
    return 0;
}

bool PlayQueueModel::processEvent(const tui::Event &ev, const tui::IVec2 &cursor) {
    if (ev.key == tui::KeyCode::ENTER) {
        json j = {
                {"message-type", "action"},
                {"action", "skip-to"},
                {"data", {
                    {"index", cursor.y}
                }}
        };
        ctx_->send(j);
    } else if (ev.key == tui::KeyCode::DEL) {
        json j = {
                {"message-type", "action"},
                {"action", "queue-remove"},
                {"data", {
                    {"index", cursor.y}
                }}
        };
        ctx_->send(j);
    }
    return false;
}

void PlayQueueModel::fullUpdate(const nlohmann::json &data) {
    tracks_.clear();
    index_ = data.at("index");
    for (auto &i : data.at("entries")) {
        tracks_.push_back(i.get<TrackMetadata>());
    }
    reset_signal();
}

void PlayQueueModel::partialUpdate(const nlohmann::json &data) {
    std::string mode = data.at("mode");
    if (mode == "insert") {
        int index = data.at("index");
        int offset = 0;
        for (auto &i : data.at("entries")) {
            auto pos = tracks_.begin() + index + offset;
            tracks_.insert(pos, i.get<TrackMetadata>());
            //itemAddedSignal(index+offset);
            ++offset;
        }

    } else if (mode == "append") {
        for (auto &i : data.at("entries")) {
            tracks_.push_back(i.get<TrackMetadata>());
            //itemAddedSignal(tracks_.size() - 1);
        }
    } else if (mode == "remove") {
        int index = data.at("index");
        int count = data.at("count");
        for (int i = 0; i < count; ++i) {
            tracks_.erase(tracks_.begin() + index);
            //itemRemovedSignal(index);
        }
        if (index <= index_) {
            index_ -= count;
        }
    } else if (mode == "clear") {
        tracks_.clear();
        index_ = 0;
        reset_signal();
    }
}

void PlayQueueModel::setIndex(int idx) {
    index_ = idx;
}
