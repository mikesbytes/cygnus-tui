//
// Created by user on 12/29/19.
//

#ifndef CYGNUS_TUI_SCREEN_HPP
#define CYGNUS_TUI_SCREEN_HPP

#include <clientcontext.hpp>
#include <cpptui/event.hpp>
#include "cpptui/ivec2.hpp"

class Interface;

class Screen {
public:
    virtual void draw() = 0;

    virtual void processEvent(const std::pair<ClientCtxPtr, nlohmann::json> &ev) {}
    virtual void processEvent(const tui::Event &ev) {}

    bool isActive() const {
        return active_;
    }

    void setActive(bool active) {
        active_ = active;
    }

    void setInterface(Interface *interface) {
        interface_ = interface;
    }

    Interface *getInterface() {
        return interface_;
    }

private:
    bool active_;
    Interface *interface_;
};


#endif //CYGNUS_TUI_SCREEN_HPP
