//
// Created by user on 12/29/19.
//

#include <fmt/format.h>
#include "playscreen.hpp"

#include "cpptui/cpptui.hpp"
#include "cpptui/textutils.hpp"
#include "metadatatools.hpp"

PlayScreen::PlayScreen(ClientCtxPtr ctx) :
    model_(ctx),
    list_(model_),
    ctx_(ctx),
    playback_time_(0)
{
    status_line_.setText("Status Line~~");
    reflow();

    // poopulate play queue
    Request r(json{{"message-type","action"},
                   {"action","queue-list"}});
    ctx->processRequest(r);
    model_.fullUpdate(r.getResponse().getJSON().at("data"));

    Request r2(json{{"message-type","action"},
                   {"action","current-track-info"}});
    ctx->processRequest(r2);
    auto j = r2.getResponse().getJSON();
    if (j.contains("data")) {
        current_track_meta_ = j.at("data").get<TrackMetadata>();
    }

    Request r3(json{{"message-type","action"},
                    {"action","get-playback-flags"}});
    ctx->processRequest(r3);
    auto j2 = r3.getResponse().getJSON();
    if (j2.contains("data")) {
        playback_state_ = j2.at("data");
    }
}

void PlayScreen::draw() {
    if (!isActive()) return;
    tui::clear();

    list_.draw();
    drawStatusLine(false);

    tui::present();
}

void PlayScreen::drawStatusLine(bool solo) {
    if (!isActive()) return;

    // solo allows us to redraw the status line(s) without redrawing the whole display
    // this is useful since the status line is updated far more frequently than the rest of the play queue
    if (solo) {
        tui::set_cursor(0, tui::window_size().y - 1);
        tui::clear_line();
        tui::set_cursor(0, tui::window_size().y - 2);
        tui::clear_line();
    }


    int width = status_line_.getSize().x;

    // build the top status line with artist - album left aligned and playback time right aligned
    std::string stat1_lhs = fmt::format("{} - {}",
            current_track_meta_.artist,
            current_track_meta_.album);
    std::string stat1_rhs = fmt::format("({:02}:{:02}/{:02}:{:02})",
            playback_time_ / 60000,
            (playback_time_ / 1000) % 60,
            current_track_meta_.length / 60000,
            (current_track_meta_.length / 1000) % 60);
    std::string spacer = std::string(width - tui::str_width(stat1_lhs) - tui::str_width(stat1_rhs), ' ');

    status_line_.setText(stat1_lhs + spacer + stat1_rhs);
    status_line_.draw();

    // build the bottom status line with track title left aligned, playback progress center aligned, and flags right aligned
    int mid_width = (width / 3) + width % 3;
    int side_width = (width - mid_width) / 2;
    int w_progress = 0;
    if (current_track_meta_.length != 0)
        w_progress = playback_time_ / (current_track_meta_.length / (mid_width - 2));

    std::string title = tui::visual_substr(current_track_meta_.title, 0, side_width);
    std::string stat2_lhs = title + std::string(side_width - tui::str_width(title), ' ');

    // build playback state indicator
    std::string stat2_rhs = "[";
    if (playback_state_.contains("shuffle") && playback_state_.at("shuffle") == true) {
        if (playback_state_.contains("shuffle-repeat") && playback_state_.at("shuffle-repeat") == true) {
            stat2_rhs += "⤨";
        } else {
            stat2_rhs += "⇆";
        }
    } else { stat2_rhs += "-"; }
    if (playback_state_.contains("consume") && playback_state_.at("consume") == true) {
        stat2_rhs += "◉";
    } else { stat2_rhs += "-"; }
    if (playback_state_.contains("single") && playback_state_.at("single") == true) {
        stat2_rhs += "①";
    } else { stat2_rhs += "-"; }
    if (playback_state_.contains("loop") && playback_state_.at("loop") == true) {
        stat2_rhs += "↻";
    } else { stat2_rhs += "-"; }
    stat2_rhs += "]";
    stat2_rhs = std::string(side_width - tui::str_width(stat2_rhs), ' ') + stat2_rhs;
    std::string stat2_mid = "<" + std::string(w_progress, '=') + std::string(std::max(mid_width - 2 - w_progress, 0), '-') + ">";

    status_line2_.setText(stat2_lhs + stat2_mid + stat2_rhs);
    status_line2_.draw();

    if (solo) {
        tui::present();
    }
}

void PlayScreen::processEvent(const std::pair<ClientCtxPtr, nlohmann::json> &ev) {
    auto [ctx, j] = ev;
    auto it = j.find("update-type");
    if (it != j.end() && it.value() == "queue") {
        model_.partialUpdate(j.at("data"));
        draw();
    } else if (it != j.end() && it.value() == "track-change") {
        model_.setIndex(j.at("data").at("index"));
        current_track_meta_ = j.at("data").get<TrackMetadata>();
        playback_time_ = 0;
        draw();
    } else if (it != j.end() && it.value() == "playback-time") {
        playback_time_ = j.at("data").at("time");
        drawStatusLine(true);
    } else if (it != j.end() && it.value() == "playback-flags") {
        playback_state_ = j.at("data");
        drawStatusLine(true);
    }
}

void PlayScreen::processEvent(const tui::Event &ev) {
    // global actions
    bool do_draw = false;
    if (ev.type == tui::EventType::RESIZE) {
        reflow();
        do_draw = true;
    }

    if (ev.str() == "p") {
        json j = {{"message-type","action"},
                  {"action","toggle-pause"}};
        ctx_->send(j);
    } else if (ev.str() == ">") {
        json j = {{"message-type", "action"},
                  {"action",       "skip-next"},
                  {"data",         {}}};
        ctx_->send(j);
    } else if (ev.str() == "c") {
        json j = {{"message-type","action"},
                  {"action","queue-clear"},
                  {"data",{}}};
        ctx_->send(j);
    } else if (isActive() && ev.str() == "z") {
        // cycle the shuffle and shuffle-repeat values
        bool shuffle = playback_state_.at("shuffle");
        bool shuffle_repeat = playback_state_.at("shuffle-repeat");
        if (shuffle) {
            if (shuffle_repeat) {
                shuffle = false;
                shuffle_repeat = false;
            } else {
                shuffle_repeat = true;
            }
        } else {
            shuffle = true;
            shuffle_repeat = false;
        }
        json j = {{"message-type","action"},
                  {"action","set-playback-flags"},
                  {"data",{{"shuffle",shuffle},
                           {"shuffle-repeat",shuffle_repeat}}}};
        ctx_->send(j);
    } else if (isActive() && ev.str() == "l") {
        bool loop = playback_state_.at("loop");
        json j = {{"message-type","action"},
                  {"action","set-playback-flags"},
                  {"data",{{"loop",!loop}}}};
        ctx_->send(j);
    } else if (isActive() && ev.str() == "s") {
        bool single = playback_state_.at("single");
        json j = {{"message-type","action"},
                  {"action","set-playback-flags"},
                  {"data",{{"single",!single}}}};
        ctx_->send(j);
    } else if (isActive() && ev.str() == "b") {
        bool consume = playback_state_.at("consume");
        json j = {{"message-type","action"},
                  {"action","set-playback-flags"},
                  {"data",{{"consume",!consume}}}};
        ctx_->send(j);
    }

    if (!isActive()) return;

    // local actions

    do_draw = list_.processEvent(ev) || do_draw;
    if (do_draw) {
        draw();
    }
}

void PlayScreen::reflow() {
    auto sz = tui::window_size();
    status_line_.setPos(tui::Pos(0, sz.y - 2));
    status_line_.setSize(tui::Size(sz.x, 1));

    status_line2_.setPos(tui::Pos(0, sz.y - 1));
    status_line2_.setSize(tui::Size(sz.x, 1));

    list_.setPos(tui::Pos(0,0));
    list_.setSize(tui::Size(sz.x, sz.y - 2 ));
}

