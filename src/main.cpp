#include <iostream>

#include "cpptui/cpptui.hpp"
#include "interface.hpp"
#include "threadpool.hpp"
#include "connectionmanager.hpp"
#include "playscreen.hpp"
#include "browsescreen.hpp"
#include "searchscreen.hpp"

int main() {
    tui::init();
    tui::hide_cursor();
    tui::present();

    Interface iface;
    ThreadPool tp;
    tp.spawnWorkers(4);

    ConnectionManager cm;
    auto ctx = cm.connectTo("127.0.0.1", 6321);

    json j = {{"message-type","action"},
              {"action","subscribe"},
              {"subscribe-to","playback-time"}};

    ctx->send(j);

    j["subscribe-to"] = "queue";
    ctx->send(j);

    j["subscribe-to"] = "track-change";
    ctx->send(j);

    j["subscribe-to"] = "playback-flags";
    ctx->send(j);

    bool running = true;
    auto netprocess = [&cm, &running]() {
        while (running) {
            cm.process(1000);
        }
    };
    tp.queueFn(netprocess);

    auto msg_fn = [&iface](const ClientCtxPtr& ctx, uint8_t tag, DataWrapper data) {
        iface.queueEvent(std::pair(ctx, data.getJSON()));
    };
    cm.processMessageSignal.connect(msg_fn);

    auto play_screen = iface.addScreen(std::make_unique<PlayScreen>(ctx));
    iface.setScreen(play_screen);

    auto browse_screen = iface.addScreen(std::make_unique<BrowseScreen>(ctx));
    auto search_screen = iface.addScreen(std::make_unique<SearchScreen>(ctx));


    while(running) {
        tui::wait_for_event();
        auto ev = tui::get_event();
        if (ev == std::nullopt) continue;

        iface.queueEvent(ev.value());
        if (!iface.getFocus()) {
            if (ev->str() == "q") running = false;
        }
    }

    tui::close();
    return 0;
}
