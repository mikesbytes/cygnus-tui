//
// Created by user on 12/29/19.
//

#ifndef CYGNUS_TUI_ITEMVIEWMODEL_HPP
#define CYGNUS_TUI_ITEMVIEWMODEL_HPP

#include <string>
#include <boost/signals2/signal.hpp>
#include <cpptui/event.hpp>
#include "cpptui/ivec2.hpp"

enum class CursorMode {
    ROW,
    ITEM
};

typedef tui::IVec2 ItemIndex;

struct Item {
public:
    std::string format;
    std::string text;
};

class ItemViewModel {
public:
    virtual std::optional<Item> getItem(const ItemIndex &idx) = 0;

    virtual int columnCount() = 0;

    virtual int rowCount() = 0;

    virtual CursorMode cursorMode() = 0;

    virtual int columnWidth(int idx, int total_width) = 0;

    virtual bool processEvent(const tui::Event &ev, const tui::IVec2 &cursor) { return false; }

    boost::signals2::signal<void()> reset_signal;
    boost::signals2::signal<void(const tui::IVec2 &)> cursor_signal;
    boost::signals2::signal<void(tui::IVec2)> scroll_signal;
};


#endif //CYGNUS_TUI_ITEMVIEWMODEL_HPP
