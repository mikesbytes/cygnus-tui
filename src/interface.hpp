//
// Created by user on 12/28/19.
//

#ifndef CYGNUS_TUI_INTERFACE_HPP
#define CYGNUS_TUI_INTERFACE_HPP

#include <variant>
#include <queue>
#include <atomic>
#include <thread>

#include "nlohmann/json.hpp"
#include "cpptui/event.hpp"
#include "notifier.hpp"
#include "widgets/labelwidget.hpp"
#include "screen.hpp"
#include "clientcontext.hpp"
#include "blockingconcurrentqueue.h"

typedef std::variant<std::pair<ClientCtxPtr, nlohmann::json>, tui::Event> IFaceEV;

class Interface {
public:
    Interface();

    void queueEvent(const IFaceEV &ev);
    bool setScreen(Screen *screen);
    bool setScreen(int idx);
    Screen *addScreen(std::unique_ptr<Screen> screen);

    /// focus disables global hotkeys
    void setFocus(bool focus);
    bool getFocus() const;

    template <typename T>
    T *addScreen(std::unique_ptr<T> screen) {
        Screen *new_screen = addScreen(std::unique_ptr<Screen>(std::move(screen)));
        return dynamic_cast<T*>(new_screen);
    }

private:
    void interfaceWorker();

    void dispatchEvent(const tui::Event &ev);
    void dispatchEvent(const std::pair<ClientCtxPtr, nlohmann::json> &ev);

    moodycamel::BlockingConcurrentQueue<IFaceEV> ev_queue_;
    //std::queue<IFaceEV> ev_queue_;
    std::mutex ev_queue_mtx_;

    Notifier event_nf_;
    std::atomic<bool> running_;
    std::unique_ptr<std::thread> iface_thread_;

    bool redraw_;
    bool focus_;

    Screen *active_screen_;
    std::vector<std::unique_ptr<Screen>> screens_;

    // widgets
    LabelWidget lbl;
};


#endif //CYGNUS_TUI_INTERFACE_HPP
