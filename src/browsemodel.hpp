//
// Created by user on 12/31/19.
//

#ifndef CYGNUS_TUI_BROWSEMODEL_HPP
#define CYGNUS_TUI_BROWSEMODEL_HPP


#include <clientcontext.hpp>
#include "itemviewmodel.hpp"

class BrowseModel : public ItemViewModel {
public:
    BrowseModel(ClientCtxPtr ctx);

    std::optional<Item> getItem(const ItemIndex &idx) override;

    int columnCount() override;

    int rowCount() override;

    CursorMode cursorMode() override;

    int columnWidth(int idx, int total_width) override;

    bool processEvent(const tui::Event &ev, const tui::IVec2 &cursor) override;

private:
    bool browseTo(const std::string &source, const std::string &path);

    bool top_level_;
    ClientCtxPtr ctx_;

    json data_;

    std::vector<std::string> sources_;
};


#endif //CYGNUS_TUI_BROWSEMODEL_HPP
