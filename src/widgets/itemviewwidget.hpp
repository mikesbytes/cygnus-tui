//
// Created by user on 12/29/19.
//

#ifndef CYGNUS_TUI_ITEMVIEWWIDGET_HPP
#define CYGNUS_TUI_ITEMVIEWWIDGET_HPP

#include "../widget.hpp"
#include "../itemviewmodel.hpp"

class ItemViewWidget : public Widget {
public:
    ItemViewWidget(ItemViewModel &model);
    void draw() override;

    bool setCursor(const ItemIndex &idx, bool scroll = true, bool force = false);
    bool moveCursor(const ItemIndex &offset);

    bool processEvent(const tui::Event &ev);

private:
    ItemIndex cursor_pos_;
    ItemIndex scroll_pos_;

    ItemViewModel &model_;
};


#endif //CYGNUS_TUI_ITEMVIEWWIDGET_HPP
