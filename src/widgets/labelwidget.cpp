//
// Created by user on 12/29/19.
//

#include "labelwidget.hpp"

#include "cpptui/cpptui.hpp"
#include "cpptui/textutils.hpp"

void LabelWidget::draw() {
    tui::set_cursor(getPos().x, getPos().y);
    tui::print(tui::visual_substr(text_, 0, getSize().x));
}

const std::string &LabelWidget::getText() const {
    return text_;
}

void LabelWidget::setText(const std::string &text) {
    text_ = text;
}
