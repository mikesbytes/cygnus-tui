//
// Created by user on 12/29/19.
//

#include <cpptui/cpptui.hpp>
#include <cpptui/textutils.hpp>
#include "itemviewwidget.hpp"

ItemViewWidget::ItemViewWidget(ItemViewModel &model) :
    model_(model),
    cursor_pos_(0,0),
    scroll_pos_(0,0)
{
    model_.cursor_signal.connect(std::bind(&ItemViewWidget::setCursor, this, std::placeholders::_1, true, true));
}

void ItemViewWidget::draw() {
    int row_start = scroll_pos_.y;
    int vis_row_count = std::min(model_.rowCount() - row_start, getSize().y);

    for (int i = 0; i < vis_row_count; ++i) {
        int col_offset = 0;
        for (int j = 0; j < model_.columnCount(); ++j) {
            Item item;
            int col_width = model_.columnWidth(j, getSize().x);
            auto item_opt = model_.getItem(ItemIndex(j, i + row_start));
            if (item_opt == std::nullopt) {
                col_offset += col_width;
                continue;
            } else {
                item = item_opt.value();
            }

            int x = col_offset + getPos().x;
            int y = i + getPos().y;

            tui::set_cursor(x,y);

            // cursor highlight
            if (cursor_pos_ == ItemIndex(j, i + row_start) ||
                    (model_.cursorMode() == CursorMode::ROW && cursor_pos_.y == i + row_start)) {
                tui::format(tui::FormatCode::REVERSE);
            }
            tui::print(item.format);
            tui::print(tui::visual_substr(item.text, 0, col_width));
            tui::format(tui::FormatCode::RESET);

            col_offset += col_width;
        }
    }
}

bool ItemViewWidget::setCursor(const ItemIndex &idx, bool scroll, bool force) {
    if (!force && model_.getItem(idx) == std::nullopt) {
        return false;
    }

    cursor_pos_ = idx;

    if (scroll) {
        if (idx.y < scroll_pos_.y) {
            scroll_pos_.y = idx.y;
        } else if (idx.y > scroll_pos_.y + getSize().y - 1) {
            scroll_pos_.y = idx.y - getSize().y + 1;
        }
    }

    return true;
}

bool ItemViewWidget::moveCursor(const ItemIndex &offset) {
    return setCursor(cursor_pos_ + offset);
}

bool ItemViewWidget::processEvent(const tui::Event &ev) {
    if (ev.key == tui::KeyCode::ARROW_DOWN) {
        return moveCursor(ItemIndex(0,1));
    } else if (ev.key == tui::KeyCode::ARROW_UP) {
        return moveCursor(ItemIndex(0, -1));
    } else if (ev.key == tui::KeyCode::ARROW_RIGHT) {
        return moveCursor(ItemIndex(1, 0));
    } else if (ev.key == tui::KeyCode::ARROW_LEFT) {
        return moveCursor(ItemIndex(-1, 0));
    } else if (ev.mouse_code == tui::MouseCode::LEFT) {
        // mouse stuff
        // check if in bounds
        if ((ev.mouse_pos.x >= getPos().x && ev.mouse_pos.x < getPos().x + getSize().x) &&
            (ev.mouse_pos.y >= getPos().y && ev.mouse_pos.y < getPos().y + getSize().y)) {
            if (model_.cursorMode() == CursorMode::ROW) {
                ItemIndex pos(0, getPos().y + ev.mouse_pos.y + scroll_pos_.y);
                return setCursor(pos);
            }
        }
    } else if (ev.mouse_code == tui::MouseCode::SCROLL_DOWN) {
        return setCursor(tui::Pos(cursor_pos_.x, cursor_pos_.y + 10));
    } else if (ev.mouse_code == tui::MouseCode::SCROLL_UP) {
        return setCursor(tui::Pos(cursor_pos_.x, cursor_pos_.y - 10));
    } else {
        return model_.processEvent(ev, cursor_pos_);
    }
    return false;
}
