//
// Created by user on 12/29/19.
//

#ifndef CYGNUS_TUI_LABELWIDGET_HPP
#define CYGNUS_TUI_LABELWIDGET_HPP

#include "../widget.hpp"

#include <string>

class LabelWidget : public Widget {
public:
    void draw() override;

    const std::string &getText() const;

    void setText(const std::string &text);

private:
    std::string text_;
};


#endif //CYGNUS_TUI_LABELWIDGET_HPP
