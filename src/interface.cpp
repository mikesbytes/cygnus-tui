//
// Created by user on 12/28/19.
//

#include "interface.hpp"
#include "cpptui/cpptui.hpp"
#include "widgets/labelwidget.hpp"

Interface::Interface() :
    running_(true),
    active_screen_(nullptr),
    focus_(false)
{
    iface_thread_ = std::make_unique<std::thread>(std::bind(&Interface::interfaceWorker, this));
    lbl.setPos(tui::Pos(0,0));
    lbl.setSize(tui::Size(80,0));
}

void Interface::queueEvent(const IFaceEV &ev) {
    ev_queue_.enqueue(ev);
}

void Interface::interfaceWorker() {
    while (running_.load()) {
        redraw_ = false;
        IFaceEV ev;
        ev_queue_.wait_dequeue(ev);

        std::visit([this](auto &&arg) {
            using T = std::decay_t<decltype(arg)>;
            this->dispatchEvent(arg);
        }, ev);
    }
}

void Interface::dispatchEvent(const std::pair<ClientCtxPtr, nlohmann::json> &ev) {
    for (auto &i : screens_) {
        i->processEvent(ev);
    }
}

void Interface::dispatchEvent(const tui::Event &ev) {
    if (!focus_) {
        if (ev.str() == "1") setScreen(0);
        else if (ev.str() == "2") setScreen(1);
        else if (ev.str() == "3") setScreen(2);
    }

    for (auto &i : screens_) {
        i->processEvent(ev);
    }
}

bool Interface::setScreen(Screen *screen) {
    if (active_screen_ != nullptr) {
        active_screen_->setActive(false);
    }

    // check if screen is in screens
    for (auto &i : screens_) {
        if (screen == &*i) {
            active_screen_ = screen;
            active_screen_->setActive(true);
            active_screen_->draw();
            return true;
        }
    }

    return false;
}

bool Interface::setScreen(int idx) {
    if (idx < 0 || idx >= screens_.size()) {
        return false;
    }
    return setScreen(&*(screens_[idx]));
}

void Interface::setFocus(bool focus) {
    focus_ = focus;
}

bool Interface::getFocus() const {
    return focus_;
}

Screen *Interface::addScreen(std::unique_ptr<Screen> screen) {
    screens_.push_back(std::move(screen));
    screens_.back()->setInterface(this);
    screens_.back()->setActive(false);
    return &*screens_.back();
}

