//
// Created by user on 1/1/20.
//

#ifndef CYGNUS_TUI_SEARCHSCREEN_HPP
#define CYGNUS_TUI_SEARCHSCREEN_HPP

#include "screen.hpp"
#include "searchmodel.hpp"
#include "widgets/itemviewwidget.hpp"
#include "widgets/labelwidget.hpp"

class SearchScreen : public Screen {
public:
    SearchScreen(ClientCtxPtr ctx);

    void draw() override;

    void processEvent(const std::pair<ClientCtxPtr, nlohmann::json> &ev) override;
    void processEvent(const tui::Event &ev) override;

private:
    void reflow();

    ClientCtxPtr ctx_;
    SearchModel model_;
    ItemViewWidget list_;
    LabelWidget search_;

    std::string search_query_;
};


#endif //CYGNUS_TUI_SEARCHSCREEN_HPP
