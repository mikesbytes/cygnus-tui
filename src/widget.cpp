//
// Created by user on 12/29/19.
//

#include "widget.hpp"

const tui::Pos &Widget::getPos() const {
    return pos_;
}

void Widget::setPos(const tui::Pos &pos) {
    pos_ = pos;
}

const tui::Size &Widget::getSize() const {
    return size_;
}

void Widget::setSize(const tui::Size &size) {
    size_ = size;
}
