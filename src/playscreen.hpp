//
// Created by user on 12/29/19.
//

#ifndef CYGNUS_TUI_PLAYSCREEN_HPP
#define CYGNUS_TUI_PLAYSCREEN_HPP

#include "screen.hpp"

#include "widgets/labelwidget.hpp"
#include "itemviewmodel.hpp"
#include "widgets/itemviewwidget.hpp"
#include "playqueuemodel.hpp"

class PlayScreen : public Screen {
public:
    PlayScreen(ClientCtxPtr ctx);

    void draw() override;
    void drawStatusLine(bool solo = true);

    void processEvent(const std::pair<ClientCtxPtr, nlohmann::json> &ev) override;
    void processEvent(const tui::Event &ev) override;

private:
    void reflow();

    tui::Size size_;

    TrackMetadata current_track_meta_;
    int playback_time_;

    LabelWidget status_line_;
    LabelWidget status_line2_;
    PlayQueueModel model_;
    ItemViewWidget list_;
    ClientCtxPtr  ctx_;
    json playback_state_;
};


#endif //CYGNUS_TUI_PLAYSCREEN_HPP
