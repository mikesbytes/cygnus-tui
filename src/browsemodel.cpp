//
// Created by user on 12/31/19.
//

#include "browsemodel.hpp"

#include <filesystem>

BrowseModel::BrowseModel(ClientCtxPtr ctx) :
    ctx_(ctx),
    top_level_(true)
{
    // populate top level
    json j = {
            {"message-type", "action"},
            {"action","library-list-sources"}
    };
    Request r(j);
    ctx_->processRequest(r);

    j = r.getResponse().getJSON();
    sources_ = j.at("data").get<std::vector<std::string>>();
}

std::optional<Item> BrowseModel::getItem(const ItemIndex &idx) {
    Item ret;
    if (top_level_ && idx.y >= 0 && idx.y < sources_.size()) {
        ret.text = sources_.at(idx.y);
        return ret;
    } else if (!top_level_ && idx.y >= 0 && idx.y < data_.at("entries").size()) {
        ret.text = data_.at("entries").at(idx.y).at("name");
        return ret;
    }

    return std::nullopt;
}

int BrowseModel::columnCount() {
    return 1;
}

int BrowseModel::rowCount() {
    if (top_level_) {
        return sources_.size();
    } else if (data_.contains("entries")) {
        return data_.at("entries").size();
    }
    return 0;
}

CursorMode BrowseModel::cursorMode() {
    return CursorMode::ROW;
}

int BrowseModel::columnWidth(int idx, int total_width) {
    return total_width;
}

bool BrowseModel::processEvent(const tui::Event &ev, const tui::IVec2 &cursor) {
    if (ev.key == tui::KeyCode::ENTER) {
        if (top_level_) {
            return browseTo(sources_.at(cursor.y), "/");
        } else if (data_.at("entries").at(cursor.y).at("type") == "dir") {
            return browseTo(data_.at("source"), data_.at("entries").at(cursor.y).at("name"));
        }
    } else if (ev.key == tui::KeyCode::BACKSPACE) {
        if (!top_level_) {
            if (data_.at("path") == "/") {
                top_level_ = true;
                cursor_signal(tui::Pos(0,0));
                return true;
            }
            std::string new_path = std::filesystem::path(data_.at("path")).parent_path();
            return browseTo(data_.at("source"), new_path);
        }
    } else if (ev.str() == " " && !top_level_) {
        std::string trackName = data_.at("entries").at(cursor.y).at("name");
        json j = {{"message-type","action"},
                  {"action", "queue-tracks"},
                  {"data", {
                                          {"source", data_.at("source")},
                                          {"path", trackName}
                                  }}};

        ctx_->send(j);

    } else if (ev.str() == "u" && !top_level_) {
        json j = {{"message-type","action"},
                  {"action","library-scan"},
                  {"data", {
                                   {"source",data_.at("source")},
                                   {"path",data_.at("path")}
                  }}};
        ctx_->send(j);
    }
    return false;
}

bool BrowseModel::browseTo(const std::string &source, const std::string &path) {
    json j = {
            {"message-type", "action"},
            {"action","list-dir"},
            {"data", {
                             {"source", source},
                             {"path", path}
            }}
    };

    Request r(j);
    ctx_->processRequest(r);
    auto res = r.getResponse().getJSON();
    if (res.at("status") != "success") return false;

    data_ = res.at("data");
    top_level_ = false;

    cursor_signal(tui::Pos(0,0));
    return true;
}
