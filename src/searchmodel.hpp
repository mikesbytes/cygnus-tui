//
// Created by user on 1/1/20.
//

#ifndef CYGNUS_TUI_SEARCHMODEL_HPP
#define CYGNUS_TUI_SEARCHMODEL_HPP


#include "itemviewmodel.hpp"
#include "clientcontext.hpp"

class SearchModel : public ItemViewModel {
public:
    SearchModel(ClientCtxPtr ctx);

    std::optional<Item> getItem(const ItemIndex &idx) override;

    int columnCount() override;

    int rowCount() override;

    CursorMode cursorMode() override;

    int columnWidth(int idx, int total_width) override;

    bool processEvent(const tui::Event &ev, const tui::IVec2 &cursor) override;

    bool doSearch(const std::string &query);

private:
    nlohmann::json data_;
    ClientCtxPtr ctx_;
    bool focused_;
};


#endif //CYGNUS_TUI_SEARCHMODEL_HPP
