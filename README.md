## Cygnus TUI

This is a TUI client for the Cygnus music server. Currently under heavy development.

### Installation

**Binary:**

Download a release from the releases page. Official builds are made on Ubuntu 18.04, but they should
work on most systems as long as libc 6 or newer is present. Using the deb is recommended on Debian
and Ubuntu systems.

**Source:**

Building from source is nearly identical to building cygnus. Conan is required for dependencies.

See the Cygnus build instructions [here](https://gitlab.com/mikesbytes/cygnus/-/blob/master/doc/building.md)

### Usage

The interface is divided into screens which can accessed via the number row on your keyboard

1: Play queue screen: Shows your current play queue and playback status

2: Library screen: Browse your music library directory style

3: Search screen: Search your library, live!

**General usage**

Arrow keys are used to navigate the interface

**Global hotkeys**

These apply as long as no text entry is currently active

- p: toggle pause state (will play if stopped)
- \>: skip to next track in queue

**Play screen**

- Enter: Switch to the track at the cursor position
- s: toggle single mode
- l: toggle loop mode
- z: cycle shuffle modes (none, shuffle with no repeats, shuffle with repeats)
- b: consume mode (removes tracks after being played)

The playback flags are indicated in the bottom right corner

**Browse screen**

- Enter: Change directory
- u: Perform a library scan on the current directory
- Space: add a track or folder to the queue

**Search screen**

- s: Start text entry for search query
- Esc: Exit text entry for search query
- Space: Add an item

Note: You must exit the query entry to add an item